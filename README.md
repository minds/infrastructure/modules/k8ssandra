# Minds K8ssandra

| Node Group | Deployment | Environments | Namespace  |
| ---------- | ---------- | ------------ | ---------- |
| stateful-k8ssandra  | Argo CD    | Sandbox, Production   | cassandra |

## Description

Helm chart for K8ssandra. Main database for the Minds backend.

## Deploy

Deployed using Argo CD. View the application in Argo, and "Sync" when changes are in master.